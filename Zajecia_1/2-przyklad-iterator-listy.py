
class IteratorListy:
	def __init__(self, indexable):
		self.indexable = indexable
		self.current_index = 0
	
	def __iter__(self):
		return self

	def __next__(self):
		if self.current_index == len(self.indexable):
			raise StopIteration
		else:
			value = self.indexable[self.current_index]
			self.current_index += 1
			return value

		
liczby = [1, 2, 3]

it = IteratorListy(liczby)

for x in it:
    print("element:", x)

# przebieg 1
# x = next(it)  --> 1
# print("element:", x)
# przebieg 2
# x = next(it)  --> 2
# print("element:", x)
# przebieg 3 
# x = next(it)  --> 3
# print("element:", x)
# przebieg 4
# try:
#    x = next(it)  --> raise StopIteration
# except StopIteration:
#   pass


# odpowiednik petli for
while True:
	try:
		x = next(it)
	except StopIteration:
		break

	print(x)

