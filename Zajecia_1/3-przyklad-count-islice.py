# 5, 6, 7, 8, 9, 10 --> count, islice

from itertools import count, islice

count_it = count(5) # 5, 6, 7, 8, 9, 10

for x in islice(count_it, 0, 6):
	print(x)
