from itertools import islice

class repeat:
	def __init__(self, elem, times=None):
		self.elem = elem
		self.times = times
		self.current = 0
	
	def __iter__(self):
		return self

	def __next__(self):
		if self.times is None or self.current < self.times:
			self.current += 1
			return self.elem
		else:
			raise StopIteration


print(list(repeat(5, 10)))

for x in islice(repeat(6), 5):
	print(x)
