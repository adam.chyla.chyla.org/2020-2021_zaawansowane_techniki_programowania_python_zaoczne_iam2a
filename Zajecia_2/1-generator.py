# 1, 2, 3, 4, 5, 6....

# funkcja generujaca, generator
def count():
    x = 0
    while True:
        yield x
        x += 1

# generator
gen = count() # <-- __iter__, __next__
print(dir(gen))

for x in gen:
	print(x)
