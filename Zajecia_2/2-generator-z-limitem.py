# 1, 2, 3, 4, 5, 6....

# funkcja generujaca, generator
def count(max_value):
    x = 0
    while True:
        if x == max_value:
            return
        yield x
        x += 1

# generator
gen = count(10) # <-- __iter__, __next__
print(dir(gen))

for x in gen:
	print(x)
