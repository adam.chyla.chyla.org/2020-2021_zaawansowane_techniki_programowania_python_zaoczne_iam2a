from itertools import islice 

def even(start=0):
	value = start
	while True:
		yield value
		value += 2 

for x in islice(even(), 10):
	print(x)

for x in islice(even(20), 10):
	print(x)
	