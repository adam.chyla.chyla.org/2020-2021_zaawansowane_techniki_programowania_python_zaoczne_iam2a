import threading
import time

# watek glowny:
#  -> utworzy nowy watek: 0, ..., 9
#  -> utworzy watek: 0, ..., 9

def count(thread_id):
	for i in range(10):
		print(f"thread_id={thread_id}; i={i}")
		time.sleep(1)

t1 = threading.Thread(target=count, args=(1,))
t2 = threading.Thread(target=count, args=(2,))

t1.start()
t2.start()

for i in range(5):
	print("Dzialam w glownym...")
	time.sleep(1)

t1.join()
t2.join()
