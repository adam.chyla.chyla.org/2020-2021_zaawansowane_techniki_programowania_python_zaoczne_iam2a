from concurrent.futures import ThreadPoolExecutor

liczby = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def dodawanie(x):
	return x + 10

with ThreadPoolExecutor(max_workers=3) as executor:
	nowa = list(executor.map(dodawanie, liczby)) 

print(nowa)
