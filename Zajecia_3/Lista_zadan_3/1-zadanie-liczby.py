from threading import Thread, Lock
import time

def count(thread_id, results, results_lock):
	results_lock.acquire()
	for i in range(10*thread_id, 10*thread_id+10):
		print(f"Watek: {thread_id}, dodaje {i}")
		results.append(i)
		
		time.sleep(thread_id)
		
	results_lock.release()


results = []
results_lock = Lock()

threads = [
	Thread(target=count, args=(1, results, results_lock)),
	Thread(target=count, args=(2, results, results_lock)),
	Thread(target=count, args=(3, results, results_lock)),
	Thread(target=count, args=(4, results, results_lock)),
]

for t in threads:
	t.start()
	
for t in threads:
	t.join()

print(results)
	