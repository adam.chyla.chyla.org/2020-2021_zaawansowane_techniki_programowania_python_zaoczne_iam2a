import multiprocessing
import time

def count(process_id):
	for i in range(20):
		print(f"process_id={process_id}; i={i}")
		time.sleep(1)

p1 = multiprocessing.Process(target=count, args=(1,))
p2 = multiprocessing.Process(target=count, args=(2,))

p1.start()
p2.start()

# ...

p1.join()
p2.join()
		