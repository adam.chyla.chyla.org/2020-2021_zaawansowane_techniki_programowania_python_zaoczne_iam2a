from concurrent.futures import ProcessPoolExecutor
import time

liczby = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def dodawanie(x):
	time.sleep(60)
	return x + 10

with ProcessPoolExecutor(max_workers=3) as executor:
	nowa = list(executor.map(dodawanie, liczby)) 

print(nowa)
