from os import getpid
import multiprocessing


def subprocess():
    print(getpid())


def main():
    print(getpid())

    processes = [
        multiprocessing.Process(target=subprocess),
        multiprocessing.Process(target=subprocess),
    ]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

if __name__ == "__main__":
    main()
