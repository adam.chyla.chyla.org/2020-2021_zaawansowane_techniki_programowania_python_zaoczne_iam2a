from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor


def potega(w):
    return w**2


def main():
    dane = []
    for _ in range(2):
        dane.append(int(input("Liczba:")))

    with ProcessPoolExecutor(max_workers=2) as pool:
        output = pool.map(potega, dane) 

    print(list(output))


if __name__ == "__main__":
    main()

