def dekorator(f):
    def inna_funkcja():
        print("inna funkcja")
        f()
        print("inna funkcja")

    return inna_funkcja

# funkcja = dekorator(funkcja)
@dekorator
def funkcja():
	print("funkcja")

funkcja()
