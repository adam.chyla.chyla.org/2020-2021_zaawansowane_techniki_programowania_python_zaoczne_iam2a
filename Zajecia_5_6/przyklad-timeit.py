import timeit

def sumowanie():
    x = sum(range(1, 100000))
    print("val: ", x)

czas_s = timeit.timeit(sumowanie, number=1)
print(czas_s)
