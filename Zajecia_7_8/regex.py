import re

# search
text = "Ala ma kota i psa"

# pattern="k.*?a"

match = re.search(pattern="k[^\s]+a",
                  string=text)

if match:
    print("search: znaleziono:", match.group(0))
else:
    print("search nie znaleziono")


# match
# kod pocztowy
# XX-XXX
# czy podany napis jest poprawnym kodem pocztowym

text = "12-922"
text = "12-922asd"

# pattern="\d\d-\d\d\d"
# pattern="\d{2}-\d{3}"

match = re.match(pattern="\d{2}-\d{3}$",
                 string=text)

if match:
    print("match: dopasowano:", match.group(0))
else:
    print("match: nie dopasowano")


# findall
# wybierz liczby XX, YYY z kodu pocztowego XX-YYY


text = "12-922"

lista = re.findall(pattern="\d+",
                   string=text)

print("findall:", lista)

    
    

    